unit BST_modulis;

interface

uses
  Bazinis, SysUtils;

const
  MAX_ELEM = 10000;

const
  DUOM_FAILAS = 'duom.txt';

const
  TUSCIAS = 0;

type
  Order = (Infix, Prefix, Postfix);

type
  Bst = record
    elem: array [1..MAX_ELEM] of BazinisTipas;
  end;


procedure InitTree(var head: Bst);
procedure Insert(var head: Bst; x: BazinisTipas);
procedure Delete(var head: Bst; key: Integer);
procedure Search(var head: Bst; key: Integer);
procedure Print(var head: Bst; o: Order);

implementation

function child_r(idx: integer): longint;
begin
  Result := 2 * idx + 1;
end;

function child_l(idx: integer): longint;
begin
  Result := 2 * idx;
end;

procedure InitTree(var head: Bst);
var
  e: BazinisTipas;
  df: Text;
  ch: char;
begin
  Assign(df, DUOM_FAILAS);
  try
    Reset(df);
    while not EOF(df) do
    begin
      ReadLn(df, e.pavadimas);
      ReadLn(df, e.gyv_skaicius);
      ReadLn(df, e.plotas);
      ReadLn(df, e.zemynas);
      ReadLn(df, ch);
      ReadLn(df, e.telefono_kodas);
      ReadLn(df);
      if ch = 'y' then
        e.ar_es := True
      else
        e.ar_es := False;
      Insert(head, e);
    end;
  except
    on E: EInOutError do
      writeln('File handling error occurred. Details: ', E.Message);
  end;
  Close(df);
end;

procedure InsertHelper(var head: Bst; x: BazinisTipas; idx: longint);
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  Assert(head.elem[idx].telefono_kodas <> x.telefono_kodas,
    'Sutampa telefono kodai');
  if head.elem[idx].telefono_kodas = TUSCIAS then
    head.elem[idx] := x
  else if head.elem[idx].telefono_kodas < x.telefono_kodas then
    InsertHelper(head, x, child_r(idx))
  else
    InsertHelper(head, x, child_l(idx));
end;

procedure Insert(var head: Bst; x: BazinisTipas);
begin
  InsertHelper(head, x, 1);
end;

procedure SearchHelper(var head: Bst; key: Integer; idx: longint);
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  if head.elem[idx].telefono_kodas = TUSCIAS then
  begin
    WriteLn(StdErr, 'Nerasta');
    Exit;
  end;
  Isvesti(head.elem[idx]);
  if head.elem[idx].telefono_kodas = key then
    Exit
  else if head.elem[idx].telefono_kodas > key then
    SearchHelper(head, key, child_l(idx))
  else
    SearchHelper(head, key, child_r(idx));
end;

procedure Search(var head: Bst; key: Integer);
begin
  SearchHelper(head, key, 1);
end;

procedure PrintInfix(var head: Bst; idx: longint);
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  if head.elem[idx].telefono_kodas = TUSCIAS then
    exit;
  PrintInfix(head, child_l(idx));
  Isvesti(head.elem[idx]);
  PrintInfix(head, child_r(idx));
end;

procedure PrintPrefix(var head: Bst; idx: longint);
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  if head.elem[idx].telefono_kodas = TUSCIAS then
    exit;
  Isvesti(head.elem[idx]);
  PrintPrefix(head, child_l(idx));
  PrintPrefix(head, child_r(idx));
end;

procedure PrintPostfix(var head: Bst; idx: longint);
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  if head.elem[idx].telefono_kodas = TUSCIAS then
    exit;
  PrintPostfix(head, child_l(idx));
  PrintPostfix(head, child_r(idx));
  Isvesti(head.elem[idx]);
end;

procedure Print(var head: Bst; o: Order);
begin
  case o of
    Infix: PrintInfix(head, 1);
    Prefix: PrintPrefix(head, 1);
    Postfix: PrintPostfix(head, 1);
  end;
end;

//Tik ne zemiau
procedure MoveSubtree(var head: Bst; ifrom: longint; ito: longint);
begin
  if head.elem[ifrom].telefono_kodas = TUSCIAS then
    Exit;
  head.elem[ito] := head.elem[ifrom];
  head.elem[ifrom].telefono_kodas := TUSCIAS;
  MoveSubtree(head, child_l(ifrom), child_l(ito));
  MoveSubtree(head, child_r(ifrom), child_r(ito));
end;

procedure DeleteHelper(var head: Bst; key: Integer; idx: longint);
var
  didz: longint;
begin
  Assert(idx <= MAX_ELEM, 'Virsytos masyvo ribos');
  if head.elem[idx].telefono_kodas = TUSCIAS then
    exit;
  if head.elem[idx].telefono_kodas > key then
    DeleteHelper(head, key, child_l(idx))
  else if head.elem[idx].telefono_kodas < key then
    DeleteHelper(head, key, child_r(idx))
  else
  begin
    if head.elem[child_l(idx)].telefono_kodas = TUSCIAS then
    begin
      //Nera vaiku
      if head.elem[child_r(idx)].telefono_kodas = TUSCIAS then
        head.elem[idx].telefono_kodas := TUSCIAS
      else //Yra desinis vaikas
        MoveSubtree(head, child_r(idx), idx);
    end
    else
    begin
      //Yra kairej vaikas
      if head.elem[child_r(idx)].telefono_kodas = TUSCIAS then
        MoveSubtree(head, child_l(idx), idx)
      //Abu vaikai OMG
      else
      begin
        didz := child_l(idx);
        while head.elem[child_r(didz)].telefono_kodas <> TUSCIAS do
          didz := child_r(didz);
        head.elem[idx] := head.elem[didz];
        DeleteHelper(head, head.elem[didz].telefono_kodas, didz);
      end;
    end;
  end;
end;

procedure Delete(var head: Bst; key: Integer);
begin
  DeleteHelper(head, key, 1);
end;

end.
