unit Bazinis;

interface


type

  Salis = record
    pavadimas: string;
    gyv_skaicius: longint;
    plotas: real;
    zemynas: string;
    ar_es: boolean;
    telefono_kodas: Integer;
  end;

  BazinisTipas = Salis;

procedure Ivesti(var x: Salis);
procedure Isvesti(x: Salis);

implementation

procedure Ivesti(var x: Salis);
var
  ch: char;
begin
  WriteLn;
  WriteLn('Ivedami salies duomenys');
  Write('Pavadinimas: ');
  ReadLn(x.pavadimas);
  Write('Gyventoju skaicius: ');
  ReadLn(x.gyv_skaicius);
  Write('Plotas: ');
  ReadLn(x.plotas);
  Write('Zemynas: ');
  ReadLn(x.zemynas);
  Write('Ar Europos Sajungoje? (y/n) ');
  ReadLn(ch);
  if ch = 'y' then
    x.ar_es := True
  else
    x.ar_es := False;
  Write('Telefono kodas: ');
  ReadLn(x.telefono_kodas);
end;

procedure Isvesti(x: Salis);
begin
  Write(x.pavadimas);
  Write(' ');
  Write(x.gyv_skaicius);
  Write(' ');
  Write(x.plotas: 0: 2);
  Write(' ');
  Write(x.zemynas);
  Write(' ');
  if x.ar_es then
    Write('y')
  else
    Write('n');
  Write(' ');
  WriteLn(x.telefono_kodas);
end;

begin
end.
