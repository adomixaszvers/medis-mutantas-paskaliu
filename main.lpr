program main;

uses bst_modulis, Bazinis;
var
  head: Bst;
  e: BazinisTipas;
  ch: char;
  key: Integer;
begin
  InitTree(head);
  While True do
  begin
       WriteLn('===========================');
       WriteLn('1. Insert(head, x)');
       WriteLn('2. Delete(head, x)');
       WriteLn('3. Search(head, x)');
       WriteLn('4. Print(head, infix)');
       WriteLn('5. Print(head, prefix)');
       WriteLn('6. Print(head, postfix)');
       WriteLn('Visa kita isjungs programa');
       Write('Jusu pasirinkimas: ');
       ReadLn(ch);
       case ch of
       '1': begin
                 Ivesti(e);
                 Insert(head, e);
            end;
       '2': begin
                 Write('key: ');
                 ReadLn(key);
                 Delete(head, key);
            end;
       '3': begin
                 Write('key: ');
                 ReadLn(key);
                 Search(head, key);
            end;
       '4': Print(head, Infix);
       '5': Print(head, Prefix);
       '6': Print(head, Postfix);
       else
         break;
       end;
  end;
end.

